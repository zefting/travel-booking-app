import { Injectable } from '@angular/core';
export interface IHotelObject {
  airport: string;
  hotelName: string;
  address: {
    City: string;
    Country: string;
    StreetAddress: string;
    PostalCode: string;
  };
  hotelRoom: {
    Description: string;
    BaseRate: string;
    Type: string;
    BedOptions: string;
  };
}
@Injectable({
  providedIn: 'root',
})
export class HotelService {
  constructor() {}
}
