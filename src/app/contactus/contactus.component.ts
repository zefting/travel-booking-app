import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css'],
})
export class ContactusComponent implements OnInit {
  constructor() {}
  model = { name: '', email: '', message: '' };
  alertSuccess = false;
  ngOnInit(): void {}
  submit() {
    console.log(this.model);
    this.model = { name: '', email: '', message: '' };
    this.alertSuccess = true;
  }
}
