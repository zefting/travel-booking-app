import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-hotel-select',
  templateUrl: './hotel-select.component.html',
  styleUrls: ['./hotel-select.component.css'],
})
export class HotelSelectComponent implements OnInit {
  @Input() hotel;
  constructor(public activeModal: NgbActiveModal) {}
  hotelRoomInfo = {};
  @Output() bookRoomEvent = new EventEmitter<any>();
  ngOnInit(): void {}
  bookHotel(room) {
    this.hotelRoomInfo = {
      hotelRoom: room,
      address: this.hotel.hotelObj.Address,
      hotelId: this.hotel.hotelObj.HotelId,
      hotelName: this.hotel.hotelObj.HotelName,
      airport: this.hotel.destinationObject.airport,
    };
    this.bookRoomEvent.emit(this.hotelRoomInfo);
  }
}
