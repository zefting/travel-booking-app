import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelSelectComponent } from './hotel-select.component';

describe('HotelSelectComponent', () => {
  let component: HotelSelectComponent;
  let fixture: ComponentFixture<HotelSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HotelSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
