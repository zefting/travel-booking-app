import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-place-order',
  templateUrl: './place-order.component.html',
  styleUrls: ['./place-order.component.css'],
})
export class PlaceOrderComponent implements OnInit {
  @Input() placeOrder;
  constructor(public activeModal: NgbActiveModal) {}
  // for this example no backend is implimentet, however the input from the parent componemt is stored in the variable below
  // and could be passed to a backend service
  orderData = {};
  ngOnInit() {}
}
