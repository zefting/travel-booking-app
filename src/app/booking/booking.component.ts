import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { distinctUntilChanged, debounceTime, map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { IdestinationObject } from './../booking.service';
import destinationsJson from './mockJson/destinationsJson.json';
import hotelsJson from './mockJson/hotelsJson.json';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HotelSelectComponent } from './../hotel-select/hotel-select.component';
import { PlaceOrderComponent } from '../place-order/place-order.component';
export interface IHotelObject {
  airport: string;
  hotelName: string;
  address: {
    City: string;
    Country: string;
    StreetAddress: string;
    PostalCode: string;
  };
  hotelRoom: {
    Description: string;
    BaseRate: string;
    Type: string;
    BedOptions: string;
  };
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css'],
})
export class BookingComponent implements OnInit {
  constructor(private modalService: NgbModal) {}
  model = { country: '', airport: '' };
  destinations = [];
  countries = [];
  clickedItem: string;
  dropdownSettings: any = {};
  destinationObject: { IdestinationObject };
  filtereddestinations = [];
  viewAirports = [];
  selectCity = false;
  selectHotel = false;
  hotels = [];
  SelectedHotel = {};
  hotelsFilteredByCity = [];
  airport: string;

  public receivedHotelBooking: {
    airport: string;
    hotelName: string;
    address: {
      City: string;
      Country: string;
      StreetAddress: string;
      PostalCode: string;
    };
    hotelRoom: {
      Description: string;
      BaseRate: string;
      Type: string;
      BedOptions: string;
    };
  };

  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'airid',
      textField: 'city',
      allowSearchFilter: true,
      closeDropDownOnSelection: true,
    };
    // mock destinations rest service
    this.destinations = destinationsJson;
    // mock hotels rest service
    this.hotels = hotelsJson;
    // mock destinations rest service
    this.countries = ['Italy', 'Spain', 'Greece'];
  }
  selectCountryItem(country: string) {
    this.hotelsFilteredByCity = [];
    this.model.country = country;
    this.viewAirports = this.destinations.filter(
      (destination) => destination.country == country
    );
    this.selectCity = true;
  }

  // onclick city
  onItemSelect(item: any) {
    this.destinationObject = this.destinations.filter(
      (destination) => destination.airid == item.airid
    )[0];
    this.selectHotel = true;
    this.hotelsFilteredByCity = this.hotels.filter(
      (hotels) => hotels.Address.City == item.city
    );
  }

  destinationSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map((term) =>
        term.length < 1
          ? []
          : this.countries
              .filter((v) => v.toLowerCase().indexOf(term.toLowerCase()) > -1)
              .slice(0, 10)
      )
    );
  open(hotel) {
    const concatHotel = {
      hotelObj: hotel,
      destinationObject: this.destinationObject,
    };
    const modalRef = this.modalService.open(HotelSelectComponent);
    modalRef.componentInstance.hotel = concatHotel;

    modalRef.componentInstance.bookRoomEvent.subscribe((receivedEntry) => {
      this.receivedHotelBooking = receivedEntry;
      console.log(this.receivedHotelBooking);
    });
  }
  openPlaceOrder(receivedHotelBooking) {
    const modalRef = this.modalService.open(PlaceOrderComponent);
    modalRef.componentInstance.placeOrder = receivedHotelBooking;
  }
}
