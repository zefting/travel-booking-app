import { Injectable } from '@angular/core';

export interface IdestinationObject{
  airid: number;
  airport: string;
  city:string;
  hotel:string;
}
@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor() { }
}
